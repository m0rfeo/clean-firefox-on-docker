FROM debian:bullseye

# Maintainer
LABEL author="m0rfeo"

# Environment variables
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

# Install firefox and additional required libs
RUN apt-get update -y && apt-get upgrade -y && apt-get install -y firefox-esr libcanberra-gtk-module libcanberra-gtk3-module libpci-dev libegl-dev \ 
#
# Required for X11 and sound support
dbus-x11 pulseaudio
#

# Define WORKDIR
WORKDIR /root

# Volume for WORKDIR
VOLUME ["/root"]

# Instruction running inside container (tail -f RUN forever)
CMD tail -f


